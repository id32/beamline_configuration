//////////////////////////////////////////////
//
// Multiplexeur Program ID32 Opiom MONO
//
//////////////////////////////////////////////

// INPUT
wire MUSST_OUTA;
wire DG2CURR;
wire FIELD;

// OUTPUT
reg RIXS_TRIG;
reg XMCD_TRIG;
reg MUSST_CH3;

// Rixs. Xmcd, Diff Hutches selector
wire [1:0]SEL_HUTCH;
wire SEL_DET;

// Input/Output Assignment
assign MUSST_OUTA = I1; // Input from MUSST OUTA channel [ZAP Point Pulse]
assign DG2CURR    = I2; // dg2curr signal
assign FIELD      = I3; // Magnetic Field signal

assign O1 = RIXS_TRIG;  // ZAP Point Pulse to OPIOM RIXS I1
assign O2 = XMCD_TRIG;  // ZAP Point Pulse ro OPIOM XMCD I1
assign O3 = MUSST_CH3;  // dg2curr or Magnetic Field

// Register Assignement
assign SEL_HUTCH[0] = IM1;  // Hutch Selector (RIXS, XMCD, DIFF, None)
assign SEL_HUTCH[1] = IM2;  // Hutch Selector (RIXS, XMCD, DIFF, None)

assign SEL_DET      = IM3;  // Detetector Selector (dg2curr or magnetic field)

// source Hutch Selector
always @(SEL_HUTCH or MUSST_OUTA)
begin
  case (SEL_HUTCH)
      2'b00 : begin
              RIXS_TRIG = MUSST_OUTA; // Rixs Hutch selected
              XMCD_TRIG = 0;
              end
      2'b01 : begin
              RIXS_TRIG = 0;
              XMCD_TRIG = MUSST_OUTA; // Xmcd/Diff Hutch selected
              end
      2'b10 : begin
              RIXS_TRIG = 0;
              XMCD_TRIG = MUSST_OUTA; // Xmcd/Diff Hutch selected
              end
      default : begin
                RIXS_TRIG = MUSST_OUTA; // Signal send To All Hutches
                XMCD_TRIG = MUSST_OUTA;
                end
  endcase
end

// source Detector Selector
always @(SEL_DET or DG2CURR or FIELD)
begin
  case (SEL_DET)
      1'b1 : 
             MUSST_CH3 = FIELD;   // Magnetic Field send on MUSST CH3
      default : 
             MUSST_CH3 = DG2CURR; // dg2curr send on MUSST CH3
  endcase
end
