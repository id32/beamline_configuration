//////////////////////////////////////////////
//
// Multiplexeur Program ID32 Opiom MONO
//
//////////////////////////////////////////////

// INPUT
wire MUSST_OUTA;
wire MUSST_OUTB;
wire DG2CURR_IN;
wire DG4CURR_IN;
wire DFMRAW_IN;

// OUTPUT
reg RIXS_TRIG;
reg XMCD_TRIG;
reg MUSST_TRIG_OUT;
reg P201_231_OUT;

// Rixs. Xmcd, Diff Hutches selector
wire [1:0]SEL_HUTCH;
wire SEL_MUSST_TRIG;

// Input/Output Assignment
assign MUSST_OUTA = I1; // Input from MUSST OUTA channel [ZAP Point Pulse]
assign MUSST_OUTB = I2; // Input from MUSST OUTB channel [ZAP Point Pulse]
assign DG2CURR_IN = I3; // dg2curr signal
assign DG4CURR_IN = I4; // dg4curr signal
assign DFMRAW_IN  = I5; // deflecting mirror signal

assign O1 = RIXS_TRIG;  // ZAP Point Pulse to OPIOM RIXS I1
assign O2 = XMCD_TRIG;  // ZAP Point Pulse ro OPIOM XMCD I1

// These signal are directly mapped to output where both ttl (-> rixs and/or xmcd) and nim (p201 231) are cabled
assign O3 = DG2CURR_IN;
assign O4 = DG4CURR_IN;
assign O5 = DFMRAW_IN;

assign O6 = P201_231_OUT; //trigger to p201

// Register Assignement
assign SEL_HUTCH[0] = IM1;  // Hutch Selector (RIXS, XMCD, DIFF, None)
assign SEL_HUTCH[1] = IM2;  // Hutch Selector (RIXS, XMCD, DIFF, None)

assign SEL_MUSST_TRIG      = IM3;  // Musst trig out Selector (ATRIG=0, BTRIG=1)

// source Hutch Selector
always @(SEL_HUTCH or MUSST_TRIG_OUT)
begin
  case (SEL_HUTCH)
      2'b00 : begin
              RIXS_TRIG = MUSST_TRIG_OUT; // Rixs Hutch selected
              XMCD_TRIG = 0;
              end
      2'b01 : begin
              RIXS_TRIG = 0;
              XMCD_TRIG = MUSST_TRIG_OUT; // Xmcd/Diff Hutch selected
              end
      default : begin
              RIXS_TRIG = MUSST_TRIG_OUT; // Signal send To All Hutches
              XMCD_TRIG = MUSST_TRIG_OUT;
              end
  endcase
end

// source Musst trig out Selector
always @(SEL_MUSST_TRIG or MUSST_OUTA or MUSST_OUTB)
begin
  case (SEL_MUSST_TRIG)
      1'b1 : begin
             MUSST_TRIG_OUT = MUSST_OUTB;
             P201_231_OUT = MUSST_OUTB;
             end
      default : begin
             MUSST_TRIG_OUT = MUSST_OUTA;
             P201_231_OUT = MUSST_OUTA;
             end
  endcase
end

               
