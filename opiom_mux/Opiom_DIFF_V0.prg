
//////////////////////////////////////////////
//
// Multiplexeur Program ID32 Opiom DIFF
//
//////////////////////////////////////////////

// INPUT
wire GAZ_CELL;
wire REF_SAMPLE;
wire DIODE_OC3;
wire PM;


// Input/Output Assignment
assign GAZ_CELL    = I1; 
assign REF_SAMPLE  = I2; 
assign DIODE_OC3   = I3; 
assign PM          = I4;

assign O1 = GAZ_CELL;
assign O2 = REF_SAMPLE;
assign O3 = DIODE_OC3;
assign O4 = PM ;
assign O5 = GAZ_CELL;
assign O6 = REF_SAMPLE;
assign O7 = DIODE_OC3;
assign O8 = PM ;
