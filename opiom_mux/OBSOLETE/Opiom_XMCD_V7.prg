
//////////////////////////////////////////////
//
// Multiplexeur Program ID32 Opiom XMCD
//
//////////////////////////////////////////////

// INPUT
wire MUSST_OUTA;
wire MUSST_MAGNET_OUTA;
wire GATEOUT_XMCD;
wire GATEOUT_DIFF;
wire FIELD;
wire PRINCETON;

// OUTPUT
reg VORTEX_TRIG;
reg XMCD_TRIG;
reg DIFF_TRIG;
reg SHUTTER;

// ZAP/Count selection
wire SEL_TRIG;

// mono/magnet zap
wire SEL_MAGNET;

// XMCD/DIFF Hutches selector
wire [1:0]SEL_HUTCH;

// Shutter mode selector
wire [1:0]SEL_SHUT;

// Input/Output Assignment
assign MUSST_OUTA        = I1; // Input from MUSST OUTA channel [ZAP Point Pulse]
assign GATEOUT_XMCD      = I2; // Input from P201 XMCD Gate Out [ Count ]
assign GATEOUT_DIFF      = I3; // Input from P201 DIFF Gate Out [ Count ]
assign FIELD             = I4; // XMCD Magnetic Field
assign PRINCETON         = I5; // Princeton Gate Out signal
assign MUSST_MAGNET_OUTA = I6; // Input from MUSST Magnet OUTA channel [ZAP point pulse]

assign O1 = VORTEX_TRIG;  // ZAP/Count to Vortex
assign O2 = XMCD_TRIG;    // ZAP pulse to XMCD P201 exit trig
assign O3 = DIFF_TRIG;    // ZAP pulse to DIFF P201 exit trig
assign O4 = FIELD;        // XMCD Magnetic Field to MONO MUSST
assign O5 = FIELD;        // XMCD Magnetic Field to XMCD P201
assign O6 = SHUTTER;      // Shutter control

// Register Assignement
assign SEL_HUTCH[1:0] = {IM2, IM1};  // Hutch selection 0=xmcd 1=diff
assign SEL_TRIG       = IM3;  // ZAP/Count selector
//assign SEL_SHUT       = IM4;  // Shutter mode selector - n=moved to IM6
assign SEL_MAGNET     = IM5;  // mono/magnet ZAP selector
assign SEL_SHUT[1:0]  = {IM7, IM6};  // XMCD or DIFF shutter 0=open 1=princeton 2=close

// source Shutter Control Selector
always @(SEL_TRIG or SEL_HUTCH or SEL_MAGNET or MUSST_MAGNET_OUTA or MUSST_OUTA or GATEOUT_XMCD)
begin
  case ({SEL_HUTCH , SEL_TRIG, SEL_MAGNET})
      4'b0010 : begin
                VORTEX_TRIG = MUSST_OUTA;
                XMCD_TRIG   = MUSST_OUTA;
                DIFF_TRIG   = 0;
                end
      4'b0011 : begin
                VORTEX_TRIG = MUSST_MAGNET_OUTA;
                XMCD_TRIG   = MUSST_MAGNET_OUTA;
                DIFF_TRIG   = 0;
                end
      4'b0110 : begin
                VORTEX_TRIG = GATEOUT_XMCD;
                XMCD_TRIG   = 0;
                DIFF_TRIG   = MUSST_OUTA;
                end
      4'b0111 : begin
                VORTEX_TRIG = GATEOUT_XMCD;
                XMCD_TRIG   = 0;
                DIFF_TRIG   = MUSST_OUTA;
                end
      default : begin
                VORTEX_TRIG = GATEOUT_XMCD;
                XMCD_TRIG   = 0;
                DIFF_TRIG   = 0;
                end
  endcase
end


// source Shutter Control Selector
always @(SEL_SHUT or PRINCETON)
begin
  case (SEL_SHUT)
      2'b01 : begin
              SHUTTER = ~PRINCETON;
             end
      2'b10 : begin
              SHUTTER = 1;
             end
       default : begin
                SHUTTER = 0;
                end
  endcase
end
