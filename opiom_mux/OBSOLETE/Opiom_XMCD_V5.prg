//////////////////////////////////////////////
//
// Multiplexeur Program ID32 Opiom XMCD
//
//////////////////////////////////////////////

// INPUT
wire MUSST_OUTA;
wire GATEOUT_XMCD;
wire GATEOUT_DIFF;
wire FIELD;
wire PRINCETON;

// OUTPUT
reg VORTEX_TRIG;
reg XMCD_TRIG;
reg DIFF_TRIG;
reg SHUTTER;

// ZAP/Count selection
wire SEL_TRIG;

// XMCD/DIFF Hutches selector
wire [1:0]SEL_HUTCH;

// Shutter mode selector
wire SEL_SHUT;

// Input/Output Assignment
assign MUSST_OUTA     = I1; // Input from MUSST OUTA channel [ZAP Point Pulse]
assign GATEOUT_XMCD   = I2; // Input from P201 XMCD Gate Out [ Count ]
assign GATEOUT_DIFF   = I3; // Input from P201 DIFF Gate Out [ Count ]
assign FIELD          = I4; // XMCD Magnetic Field
assign PRINCETON      = I5; // Princeton Gate Out signal

assign O1 = VORTEX_TRIG;  // ZAP/Count to Vortex
assign O2 = XMCD_TRIG;    // ZAP pulse to XMCD P201 exit trig
assign O3 = DIFF_TRIG;    // ZAP pulse to DIFF P201 exit trig
assign O4 = FIELD;        // XMCD Magnetic Field to MONO MUSST
assign O5 = FIELD;        // XMCD Magnetic Field to XMCD P201
assign O6 = SHUTTER;      // Shutter control

// Register Assignement
assign SEL_HUTCH[1:0] = {IM2, IM1};  // Hutch selection 0=xmcd 1=diff
assign SEL_TRIG       = IM3;  // ZAP/Count selector
assign SEL_SHUT       = IM4;  // Shutter mode selector

// source Shutter Control Selector
always @(SEL_TRIG or SEL_HUTCH or MUSST_OUTA or GATEOUT_XMCD)
begin
  case ({SEL_HUTCH , SEL_TRIG})
      3'b001 : begin
              VORTEX_TRIG = MUSST_OUTA;
              XMCD_TRIG   = MUSST_OUTA;
              DIFF_TRIG   = 0;
             end
      2'b011 : begin
              VORTEX_TRIG = GATEOUT_XMCD;
              XMCD_TRIG   = 0;
              DIFF_TRIG   = MUSST_OUTA;
              end
      default : begin
                VORTEX_TRIG = GATEOUT_XMCD;
                XMCD_TRIG   = 0;
                DIFF_TRIG   = 0;
                end
  endcase
end


// source Shutter Control Selector
always @(SEL_SHUT or PRINCETON)
begin
  case (SEL_SHUT)
      1'b1 : begin
              SHUTTER = ~PRINCETON;
             end
      default : begin
                SHUTTER = 0;
                end
  endcase
end



               

               

               

               

               

               
