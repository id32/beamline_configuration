//////////////////////////////////////////////
//
// Multiplexeur Program ID32 Opiom MONO
//
//////////////////////////////////////////////

// INPUT
wire MUSST_OUTA;

// OUTPUT
reg RIXS_TRIG;
reg XMCD_TRIG;

// Rixs. Xmcd, Diff Hutches selector
wire [1:0]SEL_HUTCH;

// Input/Output Assignment
assign MUSST_OUTA = I1; // Input from MUSST OUTA channel [ZAP Point Pulse]

assign O1 = RIXS_TRIG;  // ZAP Point Pulse to OPIOM RIXS I1
assign O2 = XMCD_TRIG;  // ZAP Point Pulse ro OPIOM XMCD I1

// Register Assignement
assign SEL_HUTCH[0] = IM1;  // Hutch Selector (RIXS, XMCD, DIFF, None)
assign SEL_HUTCH[1] = IM2;  // Hutch Selector (RIXS, XMCD, DIFF, None)

// source Shutter Control Selector
always @(SEL_HUTCH or MUSST_OUTA)
begin
  case (SEL_HUTCH)
      2'b00 : begin
              RIXS_TRIG = MUSST_OUTA; // Rixs Hutch selected
              XMCD_TRIG = 0;
              end
      2'b01 : begin
              RIXS_TRIG = 0;
              XMCD_TRIG = MUSST_OUTA; // Xmcd/Diff Hutch selected
              end
      2'b10 : begin
              RIXS_TRIG = 0;
              XMCD_TRIG = MUSST_OUTA; // Xmcd/Diff Hutch selected
              end
      default : begin
                RIXS_TRIG = MUSST_OUTA; // Signal send To All Hutches
                XMCD_TRIG = MUSST_OUTA;
                end
  endcase
end
