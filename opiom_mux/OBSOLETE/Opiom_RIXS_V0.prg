//////////////////////////////////////////////
//
// Multiplexeur Program ID32 Opiom XMCD
//
//////////////////////////////////////////////

// INPUT
wire MUSST_OUTA;
wire GATEOUT_RIXS;

// OUTPUT
reg RIXS_TRIG;

// ZAP/Count selection
wire SEL_TRIG;

// Input/Output Assignment
assign MUSST_OUTA        = I1; // Input from MUSST OUTA channel [ZAP Point Pulse]
assign GATEOUT_RIXS      = I2; // Input from P201 RIXS Gate Out [ Count ]

assign O2 = RIXS_TRIG;    // ZAP pulse to RIXS P201 exit trig

// Register Assignement
assign SEL_TRIG       = IM1;  // ZAP/Count selector

// source Shutter Control Selector
always @(SEL_TRIG or MUSST_OUTA or GATEOUT_RIXS)
begin
  case (SEL_TRIG)
      1'b1 : begin
             RIXS_TRIG   = MUSST_OUTA;
             end
      default : begin
                RIXS_TRIG   = 0;
                end
  endcase
end
      
