//////////////////////////////////////////////
//
// Multiplexeur Program ID32 Opiom XMCD
//
//////////////////////////////////////////////

// INPUT
wire MUSST_OUTA;
wire GATEOUT_RIXS;
wire PRINCETON_DIRECT;
wire PRINCETON_POLAR;

// OUTPUT
reg RIXS_TRIG;
reg SHUTTER;

// ZAP/Count selection
wire SEL_TRIG;

// Shutter mode selector
wire SEL_SHUT;

// Princeton selector
wire SEL_PRINCE;

// Input/Output Assignment
assign MUSST_OUTA        = I1; // Input from MUSST OUTA channel [ZAP Point Pulse]
assign GATEOUT_RIXS      = I2; // Input from P201 RIXS Gate Out [ Count ]
assign PRINCETON_DIRECT  = I3; // Princeton DIRECT Gate Out signal
assign PRINCETON_POLAR   = I4; // Princeton POLAR Gate Out signal

assign O2 = RIXS_TRIG;    // ZAP pulse to RIXS P201 exit trig
assign O3 = SHUTTER;      // Shutter control

// Register Assignement
assign SEL_TRIG       = IM1;  // ZAP/Count selector
assign SEL_SHUT       = IM2;  // Shutter mode selector
assign SEL_PRINCE     = IM3;  // Princeton DIRECT/POLAR selector

// source Shutter Control Selector
always @(SEL_TRIG or MUSST_OUTA or GATEOUT_RIXS)
begin
  case (SEL_TRIG)
      1'b1 : begin
             RIXS_TRIG   = MUSST_OUTA;
             end
      default : begin
                RIXS_TRIG   = 0;
                end
  endcase
end
 
// source Shutter Control Selector
always @(SEL_SHUT or SEL_PRINCE or PRINCETON_DIRECT or PRINCETON_POLAR)
begin
  case ({SEL_SHUT, SEL_PRINCE})
      1'b10 : begin
              SHUTTER = ~PRINCETON_DIRECT;
             end
      1'b11 : begin
              SHUTTER = ~PRINCETON_POLAR;
             end
      default : begin
                SHUTTER = 0;
                end
  endcase
end
            

               
