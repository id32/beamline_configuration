
from bliss.setup_globals import *

load_script("sim_mono.py")

print("")
print("Welcome to your new 'sim_mono' BLISS session !!")
print("")
print("You can now customize your 'sim_mono' session by changing files:")
print("   * /sim_mono_setup.py ")
print("   * /sim_mono.yml ")
print("   * /scripts/sim_mono.py ")
print("")

load_script("common_setup.py")

# install wait for scan/chain preset
#waitfor_obj = add_scan_preset()

#waitforrefill = waitfor_obj.waitforrefill
#waitforbeam = waitfor_obj.waitforbeam

# for this sim session we use rixs p201 counters
multiplexer_exph.switch('HUTCH_SELECT_MONO', 'RIXS')
multiplexer_rixs.switch('acq_trig_rixs', 'zap')

