
from bliss.setup_globals import *

load_script("xmcd_k6487.py")

print("")
print("Welcome to your new 'xmcd_k6487' BLISS session !! ")
print("")
print("You can now customize your 'xmcd_k6487' session by changing files:")
print("   * /xmcd_k6487_setup.py ")
print("   * /xmcd_k6487.yml ")
print("   * /scripts/xmcd_k6487.py ")
print("")

voltage = SoftAxis('voltage',k_current,position='source_value',move='source_value')
