
from bliss.setup_globals import *
from blissoda.id32.processor import Id32SpecGenProcessor as _Id32SpecGenProcessor
from blissoda.id32.hdf5_to_spec_processor import ID32Hdf5ToSpecProcessor as _ID32Hdf5ToSpecProcessor

load_script("xmcd.py")
load_script("common_setup.py")
load_script("utils.py")
print("")
print("Welcome to your new 'xmcd' BLISS session !! ")
print("")
print("You can now customize your 'xmcd' session by changing files:")
print("   * /xmcd_setup.py ")
print("   * /xmcd.yml ")
print("   * /scripts/xmcd.py ")
print("")

# from common_setup.py
prepare_musst_mono_memory()

# Ewoks workers clients
specgen = _Id32SpecGenProcessor(detectors=['dhyana95v2'])
specgen.QUEUE = "ewoksworker_lid32xmcd2"

specfileconverter = _ID32Hdf5ToSpecProcessor()

# set as default chain, hardware trigger for p201
DEFAULT_CHAIN.set_settings(chain_xmcd_trig["chain_config"])

# install wait for scan/chain preset
waitfor_obj = add_scan_preset()
waitforrefill = waitfor_obj.waitforrefill
waitforbeam = waitfor_obj.waitforbeam



# setup the keithley428 current amplifier
keithley428_setparams(kref)

    

darkcurrent = dark_xmcd.take_background
