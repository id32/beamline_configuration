
from bliss.setup_globals import *

load_script("laurent.py")
#load_script("common_setup.py")
#from id32.common_presets_simul import WaitForBeamAndOrRefill, ID32EScanPreset, ID32ChainPreset
from id32.common_presets import WaitForBeamAndOrRefill, ID32EScanPreset, ID32ChainPreset
    
def add_scan_preset():
  waitfor_obj = WaitForBeamAndOrRefill()
  #scan_preset = ID32EScanPreset(waitfor_obj)
  #escan._trig_scan.add_scan_preset(scan_preset, name="id32_waitfor")

  chain_preset = ID32ChainPreset(waitfor_obj)
  DEFAULT_CHAIN.add_preset(chain_preset, name="id32_waitfor")

  return waitfor_obj

print("")
print("Welcome to your new 'laurent' BLISS session !!")
print("")
print("You can now customize your 'laurent' session by changing files:")
print("   * /laurent_setup.py ")
print("   * /laurent.yml ")
print("   * /scripts/laurent.py ")
print("")

waitfor_obj = add_scan_preset()
waitforrefill = waitfor_obj.waitforrefill
waitforbeam = waitfor_obj.waitforbeam
