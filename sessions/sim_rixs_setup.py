
from bliss.setup_globals import *
from bliss.common.hkl import *
from bliss.common import hkl
from bliss.shell.formatters.ansi import BLUE
from bliss.common.utils import chunk_list
import tabulate

load_script("sim_rixs.py")

print("")
print("Welcome to your new 'sim_rixs' BLISS session !!")
print("")
print("You can now customize your 'sim_rixs' session by changing files:")
print("   * /sim_rixs_setup.py ")
print("   * /sim_rixs.yml ")
print("   * /scripts/sim_rixs.py ")
print("")

print(
    "You are using the Horiz. FOURC diffractometer "
    + BLUE("sim_fourc")
    + ", spec-like commands are also available:"
)
print(tabulate.tabulate(chunk_list(hkl.__all__, 8)))

