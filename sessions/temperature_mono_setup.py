
from bliss.setup_globals import *

load_script("temperature_mono.py")

print("")
print("Welcome to your new 'temperature_mono' BLISS session !!")
print("")
print("You can now customize your 'temperature_mono' session by changing files:")
print("   * /temperature_mono_setup.py ")
print("   * /temperature_mono.yml ")
print("   * /scripts/temperature_mono.py ")
print("")