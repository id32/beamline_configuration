
from bliss.setup_globals import *

load_script("fourc_diff.py")
load_script("common_setup.py")
load_script("utils.py")

print("")
print("Welcome to your new 'fourc_diff' BLISS session !! ")
print("")
print("You can now customize your 'fourc_diff' session by changing files:")
print("   * /fourc_diff_setup.py ")
print("   * /fourc_diff.yml ")
print("   * /scripts/fourc_diff.py ")
print("")

### from common_setup.py
prepare_musst_mono_memory()

# install wait for scan/chain preset
waitfor_obj = add_scan_preset()
waitforrefill = waitfor_obj.waitforrefill
waitforbeam = waitfor_obj.waitforbeam


# setup the keithley428 current amplifier: pindiode, sample and diode
keithley428_setparams(kp, ks_diff, kd_diff)

### from fourc_diff.py
accumulation_soft_on()

det = dark_diff.counters.det
mir = dark_diff.counters.Imir_diff
sam = dark_diff.counters.sam_diff
pin = dark_diff.counters.pin
grid = dark_diff.counters.grid
ref = dark_diff.counters.Iref_diff
