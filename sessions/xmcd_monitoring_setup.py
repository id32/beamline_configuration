
from bliss.setup_globals import *
from bliss.scanning.monitoring import start_monitoring,stop_all_monitoring, MONITORING_SCANS


load_script("xmcd_monitoring.py")

print("")
print("Welcome to your new 'xmcd_monitoring' BLISS session !! ")
print("")
print("You can now customize your 'xmcd_monitoring' session by changing files:")
print("   * /xmcd_monitoring_setup.py ")
print("   * /xmcd_monitoring.yml ")
print("   * /scripts/xmcd_monitoring.py ")
print("")


flint()

start_start_monitoring('xmcd_monitoring', 0.1)
