
from bliss.setup_globals import *

load_script("ls336.py")

print("")
print("Welcome to your new 'ls336' BLISS session !! ")
print("")
print("You can now customize your 'ls336' session by changing files:")
print("   * /ls336_setup.py ")
print("   * /ls336.yml ")
print("   * /scripts/ls336.py ")
print("")