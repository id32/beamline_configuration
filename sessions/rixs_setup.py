from bliss.setup_globals import *
from bliss.common.hkl import *
from bliss.common import hkl
from bliss.shell.formatters.ansi import BLUE
from bliss.common.utils import chunk_list
import tabulate


load_script("rixs.py")
load_script("common_setup.py")
load_script("utils.py")

print("")
print("Welcome to your new 'rixs' BLISS session !! ")
print("")
print("You can now customize your 'rixs' session by changing files:")
print("   * /rixs_setup.py ")
print("   * /rixs.yml ")
print("   * /scripts/rixs.py ")
print("")

from blissoda.id32.processor import Id32SpecGenProcessor as _Id32SpecGenProcessor

specgen = _Id32SpecGenProcessor(detectors=['andor1', 'andor2'])
specgen.QUEUE = "ewoksworker_lid32rixs2"


# from common_setup.py
prepare_musst_mono_memory()

# install wait for scan/chain preset
waitfor_obj = add_scan_preset()

waitforrefill = waitfor_obj.waitforrefill
waitforbeam = waitfor_obj.waitforbeam

# shortcut for fourc_mode
tth.fourc_mode = tth.controller.fourc_mode

# setup the keithley428 current amplifier
keithley428_setparams(kref, kmir, ks_rixs, kd_rixs)

print(
    "You are using the Horiz. FOURC diffractometer "
    + BLUE("fourc")
    + ", spec-like commands are also available:"
)
print(tabulate.tabulate(chunk_list(hkl.__all__, 8)))


#some useful counters aand commands
det = dark_rixs.counters.dbig_rixs
mon = dark_rixs.counters.mir_rixs
tey = cntnorm_rixs.counters.sam_n_rixs

# start He balance check every 6 hours
he_balance.check_on(6)
