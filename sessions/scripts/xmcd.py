from bliss.setup_globals import *

# some where motors
def wid():
	 wm(energy, hu70a,hu70ap, hu70apo, hu70ano, ps35b, hu70c, hu70cp, hu70cno, hu70cpo)
	 
def wps():
	wm(psho, pshg, pshall, psring, psvo, psvg, pstop, psbot)
	
def wdg1():
	wm(mdg1y, mbas1z)

def wdm():
	 wm(dmx, dmy, dmz, dmrotx, dmroty, dmrotz, dmpi)
	 
def wss():
	wm(ssho, sshg, sshall, ssring, ssvo, ssvg, sstop, ssbot)
	
def went():
	wm(entx, entz, entrotx, entgap)

def wgs():
	wm(gsgap, gsy, gsz)
	
def wpgm():
	wm(energy, mroty, mty, groty, gty, pgmz)
	
def wms():
	wm(mstop, msbot, msvo, msvg, mshall, msring, msho, mshg)
	
def wdef():
	wm(defx, defy, defz, defrotx, defroty, defrotz, defpi)
	
def wexb():
	wm(exbx, exbz, exbrotx, exbgap)
	
def wb1b():
	wm(b1btop, b1bbot, b1bvg, b1bvo, b1bhall, b1bring, b1bhg, b1bho)
	
def whrm():
	wm(hrmx, hrmy, hrmz, hrmrotx, hrmroty, hrmrotz, hrmpi, hrmb1, hrmb2)
	
def wvrm():
	wm(vrmx, vrmy, vrmz, vrmrotx, vrmroty, vrmrotz, vrmpi, vrmb1, vrmb2)
	
def wb2b():
	wm(b2btop, b2bbot, b2bvg, b2bvo, b2bhall, b2bring, b2bhg, b2bho)
	 
def wxes():
	wm(xesmask, xesgrtx, xesgrty, xesgrot, xesdetx, xesdetz, xesr1, xesr2)
	
def wsam():
	wm(sy, sz, srot)


def pshutopen(stime=3):
    multiplexer_xmcd.switch("shutter_xmcd", "open")
    sleep(stime)

def pshutclose(stime=3):
    multiplexer_xmcd.switch("shutter_xmcd", "close")
    sleep(stime)


class _myled():
    def __init__(self, device=wcid32i, channel='led', onoff={'on': 1, 'off': 0}):
        self._device = device
        self._channel = channel
        self._onoff = onoff
        self.status = self._get_status()
        
    def _get_status(self):
        return self._device.get(self._channel)
        
    def on(self):
        self._device.set(self._channel, self._onoff['on'])
        self.status = self._get_status()

    def off(self):
        self._device.set(self._channel, self._onoff['off'])
        self.status = self._get_status()

led = _myled(device=wcid32i, channel='led', onoff={'on': 1, 'off': 0})



def calib_hu70a_load(polarisation):
    coeffs = {
        'vertical': [ 3.61844814e-08, -6.08151807e-05,  4.82767996e-02,  3.05653464e-01][::-1], 
        'horizontal': [ 5.70263097e-08, -1.11673018e-04,  9.39445031e-02, -8.81399391e+00][::-1],
        'plus': [ 4.84680203e-08, -8.38655509e-05,  6.53575885e-02, -2.83245615e+00][::-1],
        'minus': [ 2.25951530e-08, -5.16029908e-05,  5.20047572e-02, -1.00938651e+00][::-1],
        }
    
    coeffs = coeffs[polarisation]
    pgm.tracking._parameters['hu70a']['param_id']['calc']['polynom'] = \
        {'E0':coeffs[0], 'E1':coeffs[1], 'E2':coeffs[2], 'E3':coeffs[3],
        'E4':0., 'E5': 0., 'E6': 0., 'min':300, 'max': 500}
    pgm.tracking._build_polynom_calib(hu70a, 'calc')
    pgm.tracking.hu70a.mode.polynom()


def calib_hu70a_unload():
    pgm.tracking.hu70a.mode.theory()

