# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import sleep

from bliss.setup_globals import *
from bliss.common.axis import Axis
from bliss.common.protocols import Scannable
from bliss.shell.getval import getval_name
from bliss.common.user_status_info import status_message
from bliss.shell.formatters.ansi import (
    PURPLE,
    CYAN,
    DARKCYAN,
    BLUE,
    GREEN,
    YELLOW,
    RED,
    UNDERLINE,
    BOLD,
)


def tweak(mot: Scannable, delta: float, time=1):
    print("Indicate direction with + (or p) or - (or n) or enter")
    print("new step size.  Type something else (or ^C) to quit.\n")
    t = "+"
    while True:
        if time != 0:
            ct(time)
        print_ansi(BLUE(f"{mot.name} = {mot.position:.3f}\n"))
        nt = input(f"Which way ({t}):")
        if nt:
            t = nt
        if t in ["+", "p"]:
            sign = 1
        else:
            sign = -1
        umvr(mot, sign * delta)


def tweak2(mot1: Scannable, delta1: float, mot2: Scannable, delta2: float, time=1):
    print("Indicate direction with + (or p) or - (or n) or enter")
    print("new step size.  Type something else (or ^C) to quit.\n")
    t = "+"
    while True:
        if time != 0:
            ct(time)
        print_ansi(
            BLUE(
                f"{mot1.name} = {mot1.position:.3f}, {mot2.name} = {mot2.position:.3f}\n"
            )
        )
        nt = input(f"Which way ({t}):")
        if nt:
            t = nt
        if t in ["+", "p"]:
            sign = 1
        else:
            sign = -1
        umvr(mot1, sign * delta1, mot2, sign * delta2)
