from bliss.setup_globals import *
from bliss.shell.getval import getval_yes_no


def led():
    val = wcid32j.get('leds')
    nval = 0 if val == 1 else 1
    print(f"setting led to {nval}")
    wcid32j.set('leds',nval)   


def ccdoff():
    # legacy from spec, staff want to switch off the shutter hw triggering
    # from the andor cameras and just keep the iceshutter opened
    fshutter_rixs.mode = fshutter_rixs.MANUAL
    fshutter_rixs.open()
    # then disable cameras
    ACTIVE_MG.disable("andor*")

def ccdon(andor1_ccd=None, andor2_ccd=None):

    # before activating hw trigger of the iceshut close it
    fshutter_rixs.mode = fshutter_rixs.MANUAL
    fshutter_rixs.close()
    fshutter_rixs.mode = fshutter_rixs.EXTERNAL
    #for the mux I3 (gate from andor) to 03 to trig the iceshutter
    multiplexer_rixs.switch("shutter_rixs","princeton")

    if andor1_ccd is None:
        resp = getval_yes_no("Enable andor1 ?", default="yes")
        if resp:
            ACTIVE_MG.enable("andor1*")
        else:
            ACTIVE_MG.disable("andor1*")
    else:
        if andor1_ccd:
            ACTIVE_MG.enable("andor1*")
        else:
            ACTIVE_MG.disable("andor1*")

    if andor2_ccd is None:
        resp = getval_yes_no("Enable andor2 ?", default="no")
        if resp:
            ACTIVE_MG.enable("andor2*")
        else:
            ACTIVE_MG.disable("andor2*")
    else:
        if andor2_ccd:
            ACTIVE_MG.enable("andor2*")
        else:
            ACTIVE_MG.disable("andor2*")

def wps():
	wm(psho,pshg,pshall,psring,psvo,psvg,pstop,psbot)
     
def wsam():
    wm(ysam,xsam,zsam,beamx,beamz)
    
def wspectro():
    wm(rtth,army,detz,detx,grtx,energy,grty,gr1roty,gr2roty,r1,r2)
    
def goto_razorblades():
    umv(xsam,1.35,ysam,-3.46,zsam,1.2,th,0,chi,-90,phi,-45)
    
def goto_ML():
    umv(xsam,-3.3108,ysam,3.0427,zsam,2.2705,th,0.25,chi,-90,phi,45)
    
def goto_ML25():
    umv(xsam,5.82,ysam,3.119,zsam,-1.243,th,1.3,chi,-90,phi,45)
    
def sample_transfer():
    umv(beamz,-1,ysam,-2,th,150,chi,-90,phi,45)
