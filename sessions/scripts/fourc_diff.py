from bliss.setup_globals import DEFAULT_CHAIN, chain_hdw_acc_dhyana, chain_soft_acc_dhyana


def accumulation_soft_on():
    # for a specific chain for dhyana in int_trigger and accumulation, master is p201 to open/close shutter ??
    DEFAULT_CHAIN.set_settings(chain_soft_acc_dhyana["chain_config"])
    print("Accumulation ON for dhyana camera in soft trig (single)")

def accumulation_off():
    # for a default chain
    DEFAULT_CHAIN.set_settings([])
    print("Accumulation OFF for dhyana camera soft trig (multi.)")

def accumulation_hdw_on():
    DEFAULT_CHAIN.set_settings(chain_hdw_acc_dhyana["chain_config"])
    multiplexer_xmcd.switch('DHYANA_TRIG_DIFF', 'ACCU')
    # later if we set fscan, a preset will exist to go back to count mode and
    # and reapply the accu mode if selected
    #current_session.env_dict['count_mux_diff'].acq_mode = 'ACCUMULATION'
    print("Accumulation ON for dhyana camera hardware trig from P201/MUSST")
    print ("Do not forget to set the max_acc_expo_time of your detector(s)")
    print ("e.g:dhyana95.accumulation.max_expo_time = 0.05")

def accumulation_hdw_off():
    DEFAULT_CHAIN.set_settings(chain_hdw_trig_dhyana["chain_config"])
    multiplexer_xmcd.switch('DHYANA_TRIG_DIFF', 'P201')
    # later if we set fscan, a preset will exist to go back to count mode and
    # and reapply the count mode if selected
    #current_session.env_dict['count_mux_diff'].acq_mode = 'COUNT'
    print ("Change acquisition mode to SINGLE for dhyana camera")

def goto_XMCD_300():
	dg4.screen_in()
	umv(gty,-58,defy,0,mroty,34906,dmpi,-18)
	pgm.gratings.XMCD_300.select()
	dg4.screen_out()
	
def goto_XMCD_900():
	dg4.screen_in()
	umv(gty,10,defy,0,mroty,34906,dmpi,-16)
	pgm.gratings.XMCD_300.select()
	dg4.screen_out()	
	
	
