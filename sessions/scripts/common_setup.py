
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from id32.common_presets import WaitForBeamAndOrRefill, ID32EScanPreset, ID32ChainPreset
    
def add_scan_preset():
  waitfor_obj = WaitForBeamAndOrRefill()
  scan_preset = ID32EScanPreset(waitfor_obj)
  escan._trig_scan.add_scan_preset(scan_preset, name="id32_waitfor")

  chain_preset = ID32ChainPreset(waitfor_obj)
  DEFAULT_CHAIN.add_preset(chain_preset, name="id32_waitfor")

  return waitfor_obj


def prepare_musst_mono_memory():
  # With SPEC and zap macros musst buffers are used differently
  #                 MCA:     size (32b values):     2048, buffers:        1
  #                 STORAGE: size (32b values):       16, buffers:    32640
  # With BLISS, we need to reset the musst to the default memory setting
  #                 MCA:     size (32b values):     2048, buffers:      128
  #                 STORAGE: size (32b values):   262144, buffers:        1
  # Program must me cleared before changing ESIZE
  musst_mono.CLEAR
  musst_mono.putget("ESIZE 262144 1")


def keithley428_setparams(*k428):
  """
  set the passed keithley428 for:
    - filter ON
    - filter rise time to 300ms (9)
    - zero check OFF
    - current suppression ON
    - reference offset 5e-8 A at gain_log10 to 8
  """
  for k in k428:
    k.FilterOn
    k.filter_rise_time = 9
    k.ZeroCheckOff
    k.CurrentSuppressOn
    k.offset(8, 5e-9)
